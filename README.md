The Spack packages in the built-in repository may not work on the NERSC systems without 
modifications. Such modifications should be pushed upstream for use by later Spack 
instances, and also added to this repo. The NERSC Spack instances will check a clone of this 
repo for new or updated packages relevant to the NERSC Spack instance.

Please see [NERSC docs for user facing spack modules](https://docs.nersc.gov/development/build-tools/spack/)
for more information.

